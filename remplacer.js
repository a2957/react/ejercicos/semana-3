function myReplace(str, before, after) {
    let helper = before;
    helper = helper.replace(/^\w/, (c) => c.toUpperCase());
    if(before === helper){
      after = after.replace(/^\w/, (c) => c.toUpperCase());
    } else {
      after = after.replace(/^\w/, (c) => c.toLowerCase());
    }

    return str.replace(before,after);
  }


const result = myReplace("Let us get back to more Coding", "Coding", "algorithms");
console.log(result);