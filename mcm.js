function MCD(arr){
    let a = arr[0]
    let b = arr[1]
    let r;
    let mcd;

    while(true){
        r = a % b
        if (r === 1) {
            mcd = r;
            return mcd
        } else if (r === 0){
            mcd = b;
            return mcd
        } else {
            a = b;
            b = r;
        }
    }
}

function MCM(arr) {
    return (arr[0]* arr[1])/MCD(arr)
}

function smallestCommons(arr) {
    arr.sort((a,b) => b - a);
    let a = arr[0]
    let b = arr[0] - 1
    const stop = arr[1]
    let result  
    while (b>= stop){
        result = MCM([a,b])
        a = result
        b -= 1
    }
    return result

}

const result = smallestCommons([5,1])
console.log(result);