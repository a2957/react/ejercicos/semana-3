function uniteUnique() {
	let result = [];
	for (let i = 0; i < arguments.length; i++) {
		const element = arguments[i];
		for (let j = 0; j < element.length; j++) {
			if (!result.includes(element[j])) {
				result.push(element[j])
			}
		}
	}
	return result;
}


const result = uniteUnique([1, 3, 2], [5, 2, 1, 4], [2, 1]);
console.log(result);